# Order-Management-demo

Order Management demo is demonstrating how to implement simple microservices with spring boot operations with a `Order` and `OrderItem` entity.

## What's inside 
- This project is based on the [Spring Boot](http://projects.spring.io/spring-boot/) project and uses these packages :
- Maven
- Spring boot Core
- Spring boot Data (JPA & H2)
- Spring boot AOP (used to know the performance of each API) 
- Spring boot Actuator (to know the metrics of the applications)
- Discovery Service(Eureka Server)
- Swagger (used to provide the api's documentation)
- Feign Client
- jacoco (code coverage)

## Modules
- This project's parent contains 4 modules.
- om-common-domain (this project contains common dependencies, entities, transfer objects and util classes. they are shared between the servers).
- om-discovery-service (this is the main eureka server).
- om-order-service (this project contains the order-service's rest APIs).
- om-order-item-service (this project contains the order-item's rest APIs).

## Installation 
- The project is created with Maven, so you just need to import it to your IDE and build the project to resolve the dependencies

- open terminal or command prompt and direct to root directory of project. Execute this maven command to build project.
```
mvn clean install
```
wait until build process has finished like this.
- Look, on coverage report under target folder there is file called jacoco-unit.exec. That file who used by Sonarqube to generate and display report about codecoverage, code quality , etc.

- download and setting [Sonarqube](https://www.sonarqube.org/downloads/) and you can use community edition for this one.
- Start Sonarqube Server
- Open your sonarqube directory and click StartSonar.batch (is depend on your operating system on your laptop)
- keep your terminal on root folder of project. and then execute this maven command to connect with Sonarqube.

```
mvn sonar:sonar
```
wait until build process has finished.

- after finished, There are two link for you to open sonarqube on browser. click that link and automatically open your browser.

## Database configuration 
Create a MySQL database with the name `springbootdb` and add the credentials to `/resources/application.properties`.  
The default ones are :

```
spring.datasource.url=jdbc:h2:mem:PUBLIC
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.hibernate.ddl-auto=create-drop
spring.jpa.show-sql=true
spring.jpa.properties.hibernate.format_sql=true
spring.jpa.properties.hibernate.validator.apply_to_ddl=false
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
```

## Usage 
Run the project through the IDE and head out to [http://localhost:5000](http://localhost:5000)
[http://localhost:5001/swagger-ui.html](http://localhost:8080/swagger-ui.html)
[http://localhost:5002/swagger-ui.html](http://localhost:5002/swagger-ui.html)
[http://localhost:5000/actuator/health](http://localhost:5000/actuator/health)
[http://localhost:5001/actuator/health](http://localhost:5001/actuator/health)
[http://localhost:5002/actuator/health](http://localhost:5002/actuator/health)

or 

run this command in the command line:
```
mvn spring-boot:run
```

