package com.order.to;

import java.util.Objects;

import com.order.entity.UserOrderItem;

public class UserOrderItemTo {

	private Long userOrderItemId;
	
	private String productCode;
	
	private String productName;
	
	private Integer quantity;
	
	private Long userOrderId;

	/**
	 * @return the userOrderId
	 */
	public Long getUserOrderId() {
		return userOrderId;
	}

	/**
	 * @param userOrderId the userOrderId to set
	 */
	public void setUserOrderId(Long userOrderId) {
		this.userOrderId = userOrderId;
	}

	/**
	 * 
	 */
	public UserOrderItemTo() {
		super();
	}

	/**
	 * @param userOrderItemId
	 * @param productCode
	 * @param productName
	 * @param quantity
	 */
	public UserOrderItemTo(Long userOrderItemId, String productCode, String productName, Integer quantity,
			Long userOrderId) {
		super();
		this.userOrderItemId = userOrderItemId;
		this.productCode = productCode;
		this.productName = productName;
		this.quantity = quantity;
		this.userOrderId = userOrderId;
	}


	public UserOrderItemTo(UserOrderItem uoi) {
		this(uoi.getUserOrderItemId(), uoi.getProductCode(), uoi.getProductName(), uoi.getQuantity(), uoi.getUserOrderId());
	}

	/**
	 * @return the userOrderItemId
	 */
	public Long getUserOrderItemId() {
		return userOrderItemId;
	}

	/**
	 * @param userOrderItemId the userOrderItemId to set
	 */
	public void setUserOrderItemId(Long userOrderItemId) {
		this.userOrderItemId = userOrderItemId;
	}

	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(userOrderItemId);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof UserOrderItemTo)) {
			return false;
		}
		UserOrderItemTo other = (UserOrderItemTo) obj;
		return Objects.equals(userOrderItemId, other.userOrderItemId);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserOrderItemTo [userOrderItemId=" + userOrderItemId + ", productCode=" + productCode + ", productName="
				+ productName + ", quantity=" + quantity + "]";
	}
	
	
}
