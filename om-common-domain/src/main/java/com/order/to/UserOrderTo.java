package com.order.to;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.order.entity.UserOrder;

public class UserOrderTo {

	private Long userOrderId;
	
	private String customerName;
	
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX", shape = Shape.STRING)
	private Date orderDate;
	
	private ShippingAddressTo shippingAddress;
	
	private List<UserOrderItemTo> orderItems;
	
	private BigDecimal totalCost;

	/**
	 * 
	 */
	public UserOrderTo() {
		super();
	}

	/**
	 * @param userOrderId
	 * @param customerName
	 * @param orderDate
	 * @param shippingAddress
	 * @param orderItems
	 * @param totalCost
	 */
	public UserOrderTo(Long userOrderId, String customerName, Date orderDate, ShippingAddressTo shippingAddress,
			List<UserOrderItemTo> orderItems, BigDecimal totalCost) {
		super();
		this.userOrderId = userOrderId;
		this.customerName = customerName;
		this.orderDate = orderDate;
		this.shippingAddress = shippingAddress;
		this.orderItems = orderItems;
		this.totalCost = totalCost;
	}

	public UserOrderTo(UserOrder userOrder, List<UserOrderItemTo> orderItems) {
		this(userOrder.getUserOrderId(), userOrder.getCustomerName(), userOrder.getOrderDate(),
				new ShippingAddressTo(userOrder.getShippingAddress()), orderItems, userOrder.getTotalCost());
	}

	/**
	 * @return the userOrderId
	 */
	public Long getUserOrderId() {
		return userOrderId;
	}

	/**
	 * @param userOrderId the userOrderId to set
	 */
	public void setUserOrderId(Long userOrderId) {
		this.userOrderId = userOrderId;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the orderDate
	 */
	public Date getOrderDate() {
		return orderDate;
	}

	/**
	 * @param orderDate the orderDate to set
	 */
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	/**
	 * @return the shippingAddress
	 */
	public ShippingAddressTo getShippingAddressTo() {
		return shippingAddress;
	}

	/**
	 * @param shippingAddress the shippingAddress to set
	 */
	public void setShippingAddressTo(ShippingAddressTo shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	/**
	 * @return the orderItems
	 */
	public List<UserOrderItemTo> getOrderItems() {
		return orderItems;
	}

	/**
	 * @param orderItems the orderItems to set
	 */
	public void setOrderItems(List<UserOrderItemTo> orderItems) {
		this.orderItems = orderItems;
	}

	/**
	 * @return the totalCost
	 */
	public BigDecimal getTotalCost() {
		return totalCost;
	}

	/**
	 * @param totalCost the totalCost to set
	 */
	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(userOrderId);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof UserOrderTo)) {
			return false;
		}
		UserOrderTo other = (UserOrderTo) obj;
		return Objects.equals(userOrderId, other.userOrderId);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserOrderTo [userOrderId=" + userOrderId + ", customerName=" + customerName + ", orderDate=" + orderDate
				+ ", shippingAddress=" + shippingAddress.toString() + ", orderItems=" + orderItems + ", totalCost=" + totalCost
				+ "]";
	}
	
	
}
