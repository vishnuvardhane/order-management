package com.order.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.order.to.UserOrderItemTo;

@Entity
@Table(name="user_order_item")
public class UserOrderItem extends Audit implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7729325361978623706L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, nullable = false)
	private Long userOrderItemId;
	
	@Column(updatable = false, nullable = false)
	private String productCode;
	
	@Column(updatable = false, nullable = false)
	private String productName;
	
	@Column(updatable = false, nullable = false)
	private Integer quantity;
	
	
	private Long userOrderId;

	/**
	 * 
	 */
	public UserOrderItem() {
		super();
	}

	/**
	 * @param userOrderItemId
	 * @param productCode
	 * @param productName
	 * @param quantity
	 */
	public UserOrderItem(Long userOrderItemId, String productCode, 
			String productName, Integer quantity, Long userOrderId) {
		super();
		this.userOrderItemId = userOrderItemId;
		this.productCode = productCode;
		this.productName = productName;
		this.quantity = quantity;
		this.userOrderId = userOrderId;
	}

	public UserOrderItem(UserOrderItemTo userOrderItem) {
		this(userOrderItem.getUserOrderItemId(), userOrderItem.getProductCode(), 
				userOrderItem.getProductName(), userOrderItem.getQuantity(), userOrderItem.getUserOrderId());
	}

	/**
	 * @return the userOrderItemId
	 */
	public Long getUserOrderItemId() {
		return userOrderItemId;
	}

	/**
	 * @param userOrderItemId the userOrderItemId to set
	 */
	public void setUserOrderItemId(Long userOrderItemId) {
		this.userOrderItemId = userOrderItemId;
	}

	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	

	/**
	 * @return the userOrderId
	 */
	public Long getUserOrderId() {
		return userOrderId;
	}

	/**
	 * @param userOrderId the userOrderId to set
	 */
	public void setUserOrderId(Long userOrderId) {
		this.userOrderId = userOrderId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(userOrderItemId);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof UserOrderItem)) {
			return false;
		}
		UserOrderItem other = (UserOrderItem) obj;
		return Objects.equals(userOrderItemId, other.userOrderItemId);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserOrderItem [userOrderItemId=" + userOrderItemId + ", productCode=" + productCode + ", productName="
				+ productName + ", quantity=" + quantity + ", userOrderId=" + userOrderId + "]";
	}

}
