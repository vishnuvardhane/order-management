package com.order.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.order.to.ShippingAddressTo;
import com.order.util.Util;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table
public class ShippingAddress extends Audit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7728866876684849325L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, nullable = false)
	private Long shippingAddressId;
	private String line1;
	private String line2;
	private String city;
	private String state;
	private String zip;
	/**
	 * 
	 */
	public ShippingAddress() {
		super();
	}
	/**
	 * @param shippingAddressId
	 * @param line1
	 * @param line2
	 * @param city
	 * @param state
	 * @param zip
	 */
	public ShippingAddress(String line1, String line2, String city, String state, String zip) {
		super();
		this.line1 = line1;
		this.line2 = line2;
		this.city = city;
		this.state = state;
		this.zip = zip;
	}
	public ShippingAddress(ShippingAddressTo shippingAddressTo) {
		this(shippingAddressTo.getLine1(), shippingAddressTo.getLine2(), 
				shippingAddressTo.getCity(), shippingAddressTo.getState(), shippingAddressTo.getZip());
	}
	/**
	 * @return the shippingAddressId
	 */
	@ApiModelProperty(hidden = true)
	public Long getShippingAddressId() {
		return shippingAddressId;
	}
	/**
	 * @param shippingAddressId the shippingAddressId to set
	 */
	public void setShippingAddressId(Long shippingAddressId) {
		this.shippingAddressId = shippingAddressId;
	}
	/**
	 * @return the line1
	 */
	public String getLine1() {
		return line1;
	}
	/**
	 * @param line1 the line1 to set
	 */
	public void setLine1(String line1) {
		if (Util.isNullOrEmptyString(line1)) {
			line1 = line1.trim();
		}
		this.line1 = line1;
	}
	/**
	 * @return the line2
	 */
	public String getLine2() {
		return line2;
	}
	/**
	 * @param line2 the line2 to set
	 */
	public void setLine2(String line2) {
		if (Util.isNullOrEmptyString(line2)) {
			line2 = line2.trim();
		}
		this.line2 = line2;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		if (Util.isNullOrEmptyString(city)) {
			city = city.trim();
		}
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		if (Util.isNullOrEmptyString(state)) {
			state = state.trim();
		}
		this.state = state;
	}
	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}
	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		if (Util.isNullOrEmptyString(zip)) {
			zip = zip.trim();
		}
		this.zip = zip;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(shippingAddressId);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ShippingAddress)) {
			return false;
		}
		ShippingAddress other = (ShippingAddress) obj;
		return Objects.equals(shippingAddressId, other.shippingAddressId);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ShippingAddress [shippingAddressId=" + shippingAddressId + ", line1=" + line1 + ", line2=" + line2
				+ ", city=" + city + ", state=" + state + ", zip=" + zip + "]";
	}

}
