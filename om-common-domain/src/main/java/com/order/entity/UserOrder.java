package com.order.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.order.to.UserOrderTo;

@Entity
@Table(name="user_order")
public class UserOrder extends Audit implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3796274157313858281L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, nullable = false)
	private Long userOrderId;
	
	@Column(updatable = true, nullable = false)
	private String customerName;
	
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX", shape = Shape.STRING)
	private Date orderDate;
	
	@JoinColumn(name = "address", insertable = true, updatable = true, nullable=false)
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private ShippingAddress shippingAddress;
	
	@Column(columnDefinition="Decimal(10,2) default '0.00'", nullable=false)
	private BigDecimal totalCost;

	/**
	 * 
	 */
	public UserOrder() {
		super();
	}

	/**
	 * @param userOrderId
	 * @param customerName
	 * @param orderDate
	 * @param shippingAddress
	 * @param orderItems
	 * @param totalCost
	 */
	public UserOrder(Long userOrderId, String customerName, Date orderDate, ShippingAddress shippingAddress,
			BigDecimal totalCost) {
		super();
		this.userOrderId = userOrderId;
		this.customerName = customerName;
		this.orderDate = orderDate;
		this.shippingAddress = shippingAddress;
		this.totalCost = totalCost;
	}

	public UserOrder(UserOrderTo userOrder) {
		this(userOrder.getUserOrderId(), userOrder.getCustomerName(), 
				userOrder.getOrderDate(), new ShippingAddress(userOrder.getShippingAddressTo()),
				 userOrder.getTotalCost());
	}
	
	public UserOrder(Long userOrderId2) {
		super();
		this.userOrderId = userOrderId2;
	}

	/**
	 * @return the userOrderId
	 */
	public Long getUserOrderId() {
		return userOrderId;
	}

	/**
	 * @param userOrderId the userOrderId to set
	 */
	public void setUserOrderId(Long userOrderId) {
		this.userOrderId = userOrderId;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the orderDate
	 */
	public Date getOrderDate() {
		return orderDate;
	}

	/**
	 * @param orderDate the orderDate to set
	 */
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	/**
	 * @return the shippingAddress
	 */
	public ShippingAddress getShippingAddress() {
		return shippingAddress;
	}

	/**
	 * @param shippingAddress the shippingAddress to set
	 */
	public void setShippingAddress(ShippingAddress shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	/**
	 * @return the totalCost
	 */
	public BigDecimal getTotalCost() {
		return totalCost;
	}

	/**
	 * @param totalCost the totalCost to set
	 */
	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(userOrderId);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof UserOrder)) {
			return false;
		}
		UserOrder other = (UserOrder) obj;
		return Objects.equals(userOrderId, other.userOrderId);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserOrder [userOrderId=" + userOrderId + ", customerName=" + customerName + ", orderDate=" + orderDate
				+ ", shippingAddress=" + shippingAddress + ", totalCost=" + totalCost
				+ "]";
	}
	
	
}
