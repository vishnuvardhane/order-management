package com.order.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonIgnore;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Audit {

	@JsonIgnore
	@CreatedBy
	@Column(updatable = false)
	private String createdBy;

	@JsonIgnore
	@CreatedDate
	@Column(updatable = false)
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date created;

	@JsonIgnore
	@LastModifiedBy
	private String modifiedBy;

	@JsonIgnore
	@LastModifiedDate
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date modified;

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreated() {
		return created;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

}

