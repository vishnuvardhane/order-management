package com.order.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * 
 * Used to serialize Java.util.Date, which is not a common JSON
 * 
 * type, so we have to create a custom serialize method;.
 *
 * 
 */

@Component

public class JsonTimeSerializer extends JsonSerializer<LocalTime> {

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:MM:SS");

	@Override
	public void serialize(LocalTime arg0, JsonGenerator arg1,
			SerializerProvider arg2) throws IOException {
		String formattedDate = dateFormat.format(arg0);

		arg1.writeString(formattedDate);

	}

}
