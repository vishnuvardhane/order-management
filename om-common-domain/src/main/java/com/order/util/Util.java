package com.order.util;

import java.util.Collection;


/**
 * 
 * @author Allu
 *
 */
public class Util {

	private Util() {
		// Do nothing..
	}

	/**
	 * Check null or empty string
	 * 
	 * @param object
	 * @return
	 */
	public static boolean isNullOrEmptyString(String object) {
		return null == object || object.trim().isEmpty();
	}

	/**
	 * Check object is null or not
	 * 
	 * @param object
	 * @return
	 */
	public static boolean isNull(Object object) {
		return null == object;
	}

	/**
	 * Check collection is null or empty
	 * 
	 * @param collection
	 * @return
	 */
	public static boolean isNullOrEmptyCollection(Collection<?> collection) {
		return collection == null || collection.isEmpty();
	}

}
