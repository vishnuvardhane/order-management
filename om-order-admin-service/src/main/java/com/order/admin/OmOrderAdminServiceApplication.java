package com.order.admin;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

@SpringBootApplication
@EnableDiscoveryClient
@EnableAdminServer
public class OmOrderAdminServiceApplication {

	public static void main(String[] args) {
		//SpringApplication.run(OmOrderAdminServiceApplication.class, args);
		new SpringApplicationBuilder(OmOrderAdminServiceApplication.class)
        .web(WebApplicationType.REACTIVE)
        .run(args);
	}

}
