package com.order.item.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.order.item.exception.OrderItemNotFoundException;
import com.order.item.service.UserOrderItemService;
import com.order.to.UserOrderItemTo;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/v1")
public class OrderItemController {

	private static final  Logger logger = LoggerFactory.getLogger(OrderItemController.class);

	@Autowired
	private UserOrderItemService userOrderItemService;
	
	@ApiOperation(value = "add new order item info")
	@ApiResponses({ // swagger - describe return status code
		@ApiResponse(code = 200, message = "Success. Request completed."),
		@ApiResponse(code = 400, message = "BAD Request"),
		@ApiResponse(code = 404, message = "Not Found - resource doesn't exist."),
		@ApiResponse(code = 500, message = "Internal Server error.") })
	@PostMapping("/orderitem")
	public ResponseEntity<UserOrderItemTo> saveNewOrderitem(@Valid @RequestBody UserOrderItemTo userOrderTo) throws OrderItemNotFoundException {
		try {
			return new ResponseEntity<>(userOrderItemService.save(userOrderTo), HttpStatus.CREATED);
		} catch (OrderItemNotFoundException e) {
			logger.error("Exception occured while adding new order : ", e);
			throw e;
		}
	}
	
	@ApiOperation(value = "get order item info by id")
	@ApiResponses({ // swagger - describe return status code
		@ApiResponse(code = 200, message = "Success. Request completed."),
		@ApiResponse(code = 400, message = "BAD Request"),
		@ApiResponse(code = 404, message = "Not Found - resource doesn't exist."),
		@ApiResponse(code = 500, message = "Internal Server error.") })
	@GetMapping("/orderitem/{orderItemId}")
	public ResponseEntity<UserOrderItemTo> orderInfobyId(@PathVariable("orderItemId") Long orderItemId) throws OrderItemNotFoundException {
		try {
			return new ResponseEntity<>(userOrderItemService.get(orderItemId),HttpStatus.OK);
		} catch (OrderItemNotFoundException onfe) {
			logger.error("Exception occured while retrieving order.{}", onfe);
			throw onfe;
		}
	}
	
	@ApiOperation(value = "get order item info by id")
	@ApiResponses({ // swagger - describe return status code
		@ApiResponse(code = 200, message = "Success. Request completed."),
		@ApiResponse(code = 400, message = "BAD Request"),
		@ApiResponse(code = 404, message = "Not Found - resource doesn't exist."),
		@ApiResponse(code = 500, message = "Internal Server error.") })
	@GetMapping("/orderitem")
	public ResponseEntity<List<UserOrderItemTo>> orderItemInfobyOrderId(@RequestParam("orderId") Long orderId) throws OrderItemNotFoundException {
		try {
			return new ResponseEntity<>(userOrderItemService.getByOrderId(orderId),
					HttpStatus.OK);
		} catch (OrderItemNotFoundException onfe) {
			logger.error("Exception occured while retrieving order.{}", onfe);
			throw onfe;
		}
	}

}
