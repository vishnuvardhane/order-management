package com.order.item.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.order.entity.UserOrderItem;

@Repository
public interface UserOrderItemRepository extends JpaRepository<UserOrderItem, Long> {

	List<UserOrderItem> findByUserOrderId(Long orderId);

}
