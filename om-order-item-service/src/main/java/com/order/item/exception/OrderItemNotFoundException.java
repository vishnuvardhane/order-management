package com.order.item.exception;

public class OrderItemNotFoundException extends Exception {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -5627202314411111766L;

	public OrderItemNotFoundException(String message) {
		super(message);
	}
}
