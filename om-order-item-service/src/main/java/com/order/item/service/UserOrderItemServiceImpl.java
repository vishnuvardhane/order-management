package com.order.item.service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.order.entity.UserOrderItem;
import com.order.item.exception.OrderItemNotFoundException;
import com.order.item.repository.UserOrderItemRepository;
import com.order.to.UserOrderItemTo;
import com.order.util.Util;

@Service
public class UserOrderItemServiceImpl implements UserOrderItemService {
	private static final Logger logger = LoggerFactory.getLogger(UserOrderItemServiceImpl.class);

	@Autowired
	private UserOrderItemRepository userOrderItemRepository;
	
	/**
	 * Saving new order item
	 */
	@Transactional
	@Override
	public UserOrderItemTo save(UserOrderItemTo userOrderItemTo) throws OrderItemNotFoundException {
		
		try {
			UserOrderItem  uoi = new UserOrderItem(userOrderItemTo);
			userOrderItemRepository.save(uoi);
			
			userOrderItemTo.setUserOrderItemId(uoi.getUserOrderItemId());
			return userOrderItemTo;
		}catch(Exception e) {
			logger.error("Exception occured while saving the order item", e.getMessage());
			throw new OrderItemNotFoundException(e.getMessage());
		}
	}

	/**
	 * Retrieving orderItem info by id
	 */
	@Override
	public UserOrderItemTo get(Long orderItemId) throws OrderItemNotFoundException {
		logger.info("retriving order item by id {}", orderItemId);
		if(Objects.isNull(orderItemId)) {
			throw new OrderItemNotFoundException("order item id must not be null.");
		}
		
		try {
			UserOrderItem orderItem = userOrderItemRepository.findById(orderItemId).orElse(null);
			if(Objects.isNull(orderItem)) {
				throw new OrderItemNotFoundException("order item not found.");
			}
			return new UserOrderItemTo(orderItem);
		}catch(Exception e) {
			logger.error("Exception occured while retrieving the order item by order item id : " + orderItemId, e.getMessage());
			throw new OrderItemNotFoundException(e.getMessage());
		}
	}

	@Override
	public List<UserOrderItemTo> getByOrderId(Long orderId) throws OrderItemNotFoundException {
		logger.info("retriving order items by order id {}", orderId);
		if(Objects.isNull(orderId)) {
			throw new OrderItemNotFoundException("order id is must not be null.");
		}
		List<UserOrderItemTo> orderItemsTos = null;
		try {
			List<UserOrderItem> orderItems = userOrderItemRepository.findByUserOrderId(orderId);
			if(!Util.isNullOrEmptyCollection(orderItems)) {
				orderItemsTos = orderItems.stream().filter(Objects::nonNull).map(UserOrderItemTo::new).collect(Collectors.toList());
			}
			
			return orderItemsTos;
			
		}catch(Exception e) {
			logger.error("Exception occured while retrieving the order items by order id : " + orderId, e.getMessage());
			throw new OrderItemNotFoundException(e.getMessage());
		}
	}

}
