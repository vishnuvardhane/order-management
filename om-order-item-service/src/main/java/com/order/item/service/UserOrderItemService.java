/**
 * 
 */
package com.order.item.service;

import java.util.List;

import com.order.item.exception.OrderItemNotFoundException;
import com.order.to.UserOrderItemTo;

/**
 * @author lenovo
 *
 */
public interface UserOrderItemService {
	UserOrderItemTo save(UserOrderItemTo userOrder) throws OrderItemNotFoundException;

	UserOrderItemTo get(Long orderItemId) throws OrderItemNotFoundException;

	List<UserOrderItemTo> getByOrderId(Long orderId) throws OrderItemNotFoundException;
}
