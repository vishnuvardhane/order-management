package com.order.item.controller.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.order.item.OrderItemServiceApplication;
import com.order.item.service.UserOrderItemService;
import com.order.to.UserOrderItemTo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrderItemServiceApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OrderItemControllerTest {

	@Mock
	private UserOrderItemService userOrderItemService;
	
	UserOrderItemTo mockUserOrderItemTo;
	
	@Autowired
    private TestRestTemplate restTemplate;

    @Value("${local.server.port:5002}")
    private int port;

    private String getRootUrl() {
        return "http://localhost:" + port + "/api/v1";
    }

    List<UserOrderItemTo> mockUOItemTos = null;
	@Before
	public void init() {
		mockUserOrderItemTo = new UserOrderItemTo(1L, "laptop", "lenovo", 1, 1L);
		
		mockUOItemTos = new ArrayList<UserOrderItemTo>();
		mockUOItemTos.add(mockUserOrderItemTo);
	}
			
	@Test
	public void testSaveNewOrder() throws Exception {
		

		Mockito.when(userOrderItemService.save(Mockito.any(UserOrderItemTo.class))).thenReturn(mockUserOrderItemTo);
		
		ResponseEntity<UserOrderItemTo> postResponse = restTemplate.postForEntity(getRootUrl() + "/orderitem", mockUserOrderItemTo, UserOrderItemTo.class);

		assertEquals(HttpStatus.CREATED, postResponse.getStatusCode());

	}
	
	@Test
	public void testGetOrderItemInfobyOrderItemId() throws Exception {
		
		Mockito.when(userOrderItemService.get(Mockito.anyLong())).thenReturn(mockUserOrderItemTo);
		
		UserOrderItemTo orderItemTo = restTemplate.getForObject(getRootUrl() + "/orderitem/1", UserOrderItemTo.class);
        assertNotNull(orderItemTo);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testGetOrderItemsInfobyOrderId() throws Exception {
		
		
		Mockito.when(userOrderItemService.getByOrderId(Mockito.anyLong())).thenReturn(mockUOItemTos);
		
		List<UserOrderItemTo> uoItemTos = (List<UserOrderItemTo>) restTemplate.getForObject(getRootUrl() + "/orderitem?orderId=1", Object.class);
        assertNotNull(uoItemTos);
	}
	
}
