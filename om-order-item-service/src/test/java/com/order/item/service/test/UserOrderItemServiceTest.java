/**
 * 
 */
package com.order.item.service.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.order.entity.UserOrderItem;
import com.order.item.exception.OrderItemNotFoundException;
import com.order.item.repository.UserOrderItemRepository;
import com.order.item.service.UserOrderItemService;
import com.order.item.service.UserOrderItemServiceImpl;
import com.order.to.UserOrderItemTo;

/**
 * @author lenovo
 *
 */
@RunWith(SpringRunner.class)
public class UserOrderItemServiceTest {

	@Autowired
    private UserOrderItemService userOrderItemService;
 
	@MockBean
    private UserOrderItemRepository orderItemRepository;
    
    @TestConfiguration
    static class UserOrderItemServiceImplTestContextConfiguration {
 
        @Bean
        public UserOrderItemService userOrderItemService() {
            return new UserOrderItemServiceImpl();
        }
    }
    
    UserOrderItemTo mockUserOrderItemTo;
    List<UserOrderItemTo> mockUOItemTos = null;
	@Before
	public void init() {
		mockUserOrderItemTo = new UserOrderItemTo(1L, "laptop", "lenovo", 1, 1L);
		
		mockUOItemTos = new ArrayList<UserOrderItemTo>();
		mockUOItemTos.add(mockUserOrderItemTo);
	}
	
	@Test
	public void testSave() throws OrderItemNotFoundException {
		UserOrderItem uoi = new UserOrderItem(mockUserOrderItemTo);
		Mockito.when(orderItemRepository.save(uoi)).thenReturn(uoi);
		UserOrderItemTo uoiTo = userOrderItemService.save(mockUserOrderItemTo);
		assertEquals(mockUserOrderItemTo, uoiTo);
		verify(orderItemRepository, times(1)).save(uoi);
	}

	@Test
	public void testGet() throws OrderItemNotFoundException {
		UserOrderItem uoi = new UserOrderItem(mockUserOrderItemTo);
		Optional<UserOrderItem> op = Optional.of(uoi);
		Mockito.when(orderItemRepository.findById(Mockito.anyLong())).thenReturn(op);
		UserOrderItemTo uoiTo = userOrderItemService.get(1L);
		assertEquals(mockUserOrderItemTo, uoiTo);
		verify(orderItemRepository, times(1)).findById(1L);
	}

	@Test
	public void testGetByOrderId() throws OrderItemNotFoundException {
		UserOrderItem uoi = new UserOrderItem(mockUserOrderItemTo);
		List<UserOrderItem> mockUOIs = new ArrayList<>();
		mockUOIs.add(uoi);
		Mockito.when(orderItemRepository.findByUserOrderId(Mockito.anyLong())).thenReturn(mockUOIs);
		List<UserOrderItemTo> uoiTos = userOrderItemService.getByOrderId(1L);
		assertEquals(mockUOItemTos, uoiTos);
		verify(orderItemRepository, times(1)).findByUserOrderId(1L);
	}
}
