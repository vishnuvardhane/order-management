package com.order.controller.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.order.OrderServiceApplication;
import com.order.feignclient.OrderItemServiceProxy;
import com.order.service.UserOrderService;
import com.order.to.ShippingAddressTo;
import com.order.to.UserOrderItemTo;
import com.order.to.UserOrderTo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrderServiceApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OrderControllerTest {

	@Mock
	private UserOrderService userOrderService;
	
	@MockBean
	private OrderItemServiceProxy orderItemServiceProxy;
	
	UserOrderTo mockOrderTo;
	List<UserOrderItemTo> mockUOItemTos = null;
	UserOrderItemTo mockUserOrderItemTo;
	@Before
	public void init() {
		mockUserOrderItemTo = new UserOrderItemTo(1L, "laptop", "lenovo", 1, 1L);
		mockUOItemTos = new ArrayList<>();
		mockUOItemTos.add(mockUserOrderItemTo);
		
		mockOrderTo = new UserOrderTo(1L, "vishnu", new Date(), 
				new ShippingAddressTo("kp", "seshadri nagar", "hyd", "telangana", "506349"), 
				mockUOItemTos, BigDecimal.valueOf(26581.0D));
		
	}		
	
	
	
	@Autowired
    private TestRestTemplate restTemplate;

    @Value("${local.server.port:5001}")
    private int port;

    private String getRootUrl() {
        return "http://localhost:" + port + "/api/v1";
    }

    
		
	@Test
	public void testSaveNewOrder() throws Exception {
		

		Mockito.when(userOrderService.save(Mockito.any(UserOrderTo.class))).thenReturn(mockOrderTo);
		ResponseEntity<UserOrderItemTo> responseEntity = new ResponseEntity<UserOrderItemTo>(mockUserOrderItemTo, HttpStatus.CREATED);
		Mockito.when(orderItemServiceProxy.saveNewOrderItem(mockUserOrderItemTo)).thenReturn(responseEntity);
		
		ResponseEntity<UserOrderTo> postResponse = restTemplate.postForEntity(getRootUrl() + "/order", mockOrderTo, UserOrderTo.class);

		assertEquals(HttpStatus.CREATED, postResponse.getStatusCode());

	}
	
	@Test
	public void testGetOrderInfobyOrderId() throws Exception {
		
		Mockito.when(userOrderService.get(Mockito.anyLong())).thenReturn(mockOrderTo);
		
		UserOrderTo orderTo = restTemplate.getForObject(getRootUrl() + "/order/1", UserOrderTo.class);
        assertNotNull(orderTo);
	}
	
	@Test
	public void testGetOrderItemsInfobyOrderId() throws Exception {
		
		Mockito.when(userOrderService.getAllItemsByOrderId(Mockito.anyLong())).thenReturn(mockUOItemTos);
		
		Object uoItemTos = restTemplate.getForObject(getRootUrl() + "/order/items/1", Object.class);
        assertNotNull(uoItemTos);
	}
	
}
