/**
 * 
 */
package com.order.service.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.order.entity.UserOrder;
import com.order.entity.UserOrderItem;
import com.order.exception.OrderNotFoundException;
import com.order.feignclient.OrderItemServiceProxy;
import com.order.repository.UserOrderRepository;
import com.order.service.UserOrderService;
import com.order.service.UserOrderServiceImpl;
import com.order.to.ShippingAddressTo;
import com.order.to.UserOrderItemTo;
import com.order.to.UserOrderTo;

/**
 * @author lenovo
 *
 */
@RunWith(SpringRunner.class)
public class UserOrderServiceTest {

	@Autowired
    private UserOrderService userOrderService;
 
	@MockBean
    private UserOrderRepository orderRepository;
	
	@MockBean
	private OrderItemServiceProxy orderItemServiceProxy;
    
    @TestConfiguration
    static class UserOrderServiceImplTestContextConfiguration {
 
        @Bean
        public UserOrderService userOrderService() {
            return new UserOrderServiceImpl();
        }
    }
    
    UserOrderTo mockOrderTo;
    UserOrderItemTo mockUserOrderItemTo;
	List<UserOrderItemTo> mockUOItemTos = null;
	@Before
	public void init() {
		mockUserOrderItemTo = new UserOrderItemTo(1L, "laptop", "lenovo", 1, 1L);
		mockUOItemTos = new ArrayList<>();
		mockUOItemTos.add(mockUserOrderItemTo);
		
		mockOrderTo = new UserOrderTo(1L, "vishnu", new Date(), 
				new ShippingAddressTo("kp", "seshadri nagar", "hyd", "telangana", "506349"), 
				mockUOItemTos, BigDecimal.valueOf(26581.0D));
		
	}
	
	@Test
	public void testSave() throws OrderNotFoundException {
		UserOrder uo = new UserOrder(mockOrderTo);
		ResponseEntity<UserOrderItemTo> responseEntity = new ResponseEntity<UserOrderItemTo>(mockUserOrderItemTo, HttpStatus.CREATED);
		
		Mockito.when(orderRepository.save(uo)).thenReturn(uo);
		Mockito.when(orderItemServiceProxy.saveNewOrderItem(mockUserOrderItemTo)).thenReturn(responseEntity);
		
		UserOrderTo uoTo = userOrderService.save(mockOrderTo);
		assertEquals(mockOrderTo, uoTo);
		verify(orderRepository, times(1)).save(uo);
	}

	@Test
	public void testGet() throws OrderNotFoundException {
		UserOrder uo = new UserOrder(mockOrderTo);
		Optional<UserOrder> op = Optional.of(uo);
		Mockito.when(orderRepository.findById(Mockito.anyLong())).thenReturn(op);
		
		
		UserOrderItem uoi = new UserOrderItem(mockUserOrderItemTo);
		List<UserOrderItem> mockUOIs = new ArrayList<>();
		mockUOIs.add(uoi);
		
		ResponseEntity<Object> responseEntity = new ResponseEntity<Object>(mockUOItemTos, HttpStatus.OK);
		Mockito.when(orderItemServiceProxy.orderItemInfobyOrderId(Mockito.anyLong())).thenReturn(responseEntity);
		
		UserOrderTo uoTo = userOrderService.get(1L);
		assertEquals(mockOrderTo, uoTo);
		verify(orderRepository, times(1)).findById(1L);
	}

	@Test
	public void testGetByOrderId() throws OrderNotFoundException {
		UserOrderItem uoi = new UserOrderItem(mockUserOrderItemTo);
		List<UserOrderItem> mockUOIs = new ArrayList<>();
		mockUOIs.add(uoi);
		
		ResponseEntity<Object> responseEntity = new ResponseEntity<Object>(mockUOItemTos, HttpStatus.OK);
		
		Mockito.when(orderItemServiceProxy.orderItemInfobyOrderId(Mockito.anyLong())).thenReturn(responseEntity);
		Object uoiTos = userOrderService.getAllItemsByOrderId(1L);
		assertNotNull( uoiTos);
	}
}
