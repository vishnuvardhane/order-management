package com.order.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.order.to.UserOrderItemTo;

@FeignClient(name = "${order_item_service_name}", url = "${domain_url}")
public interface OrderItemServiceProxy {

	@PostMapping(value = "/api/v1/orderitem")
	public ResponseEntity<UserOrderItemTo> saveNewOrderItem(@RequestBody UserOrderItemTo orderItemTo);
	
	@GetMapping("/api/v1/orderitem/{orderItemId}")
	public ResponseEntity<UserOrderItemTo> orderInfobyId(@PathVariable("orderItemId") Long orderItemId);
	
	@GetMapping("/api/v1/orderitem")
	public ResponseEntity<Object> orderItemInfobyOrderId(@RequestParam("orderId") Long orderId);

}
