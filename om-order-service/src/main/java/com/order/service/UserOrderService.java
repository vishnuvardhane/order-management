/**
 * 
 */
package com.order.service;

import com.order.exception.OrderNotFoundException;
import com.order.to.UserOrderTo;

/**
 * @author lenovo
 *
 */
public interface UserOrderService {

	UserOrderTo save(UserOrderTo userOrder) throws OrderNotFoundException;
	
	UserOrderTo get(Long orderId) throws OrderNotFoundException;
	
	Object getAllItemsByOrderId(Long orderId) throws OrderNotFoundException;

}
