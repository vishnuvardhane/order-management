/**
 * 
 */
package com.order.service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.order.entity.UserOrder;
import com.order.exception.OrderNotFoundException;
import com.order.feignclient.OrderItemServiceProxy;
import com.order.repository.UserOrderRepository;
import com.order.to.CustomeErrorResponse;
import com.order.to.UserOrderItemTo;
import com.order.to.UserOrderTo;
import com.order.util.Util;

/**
 * @author lenovo
 *
 */
@Service
public class UserOrderServiceImpl implements UserOrderService {
	
	private static final Logger logger = LoggerFactory.getLogger(UserOrderServiceImpl.class);

	@Autowired
	private UserOrderRepository userOrderRepository;
	
	@Autowired
	private OrderItemServiceProxy orderItemServiceProxy;
	
	
	/* (non-Javadoc)
	 * @see com.order.service.UserOrderService#save(com.order.entity.UserOrder)
	 */
	@Transactional
	@Override
	public UserOrderTo save(UserOrderTo userOrderTo) throws OrderNotFoundException {
		logger.info("saving userOrderTo {}", userOrderTo);
		if(Objects.isNull(userOrderTo)
				|| Objects.isNull(userOrderTo.getCustomerName())
				|| Objects.isNull(userOrderTo.getShippingAddressTo())
				|| Objects.isNull(userOrderTo.getOrderItems())
				|| Objects.isNull(userOrderTo.getTotalCost())) {
			throw new OrderNotFoundException("customer name or shipping address or cost or order items are must not be null.");
		}
		
		List<UserOrderItemTo> userOrderItemsTos = userOrderTo.getOrderItems();
		if(Util.isNullOrEmptyCollection(userOrderItemsTos)) {
			throw new OrderNotFoundException("user order tiems can not be empty.");
		}
		for (UserOrderItemTo userOrderItemTo : userOrderItemsTos) {
			if(Objects.isNull(userOrderItemTo.getProductCode())
					|| Objects.isNull(userOrderItemTo.getProductName())
					|| Objects.isNull(userOrderItemTo.getQuantity())
					) {
				throw new OrderNotFoundException("product code or name or quantity or order ids are must not be null.");
			}
		}
				
		try {
			userOrderTo.setOrderDate(new Date());
			UserOrder userOrder = new UserOrder(userOrderTo);
			
			userOrder = userOrderRepository.save(userOrder);
			Long userOrderId = userOrder.getUserOrderId();
			for (UserOrderItemTo userOrderItemTo : userOrderItemsTos) {
				userOrderItemTo.setUserOrderId(userOrderId);
			}
			List<UserOrderItemTo> orderItemsTo = userOrderItemsTos.stream()
					.map(uoi -> orderItemServiceProxy.saveNewOrderItem(uoi).getBody()).collect(Collectors.toList());
			
			userOrderTo.setUserOrderId(userOrderId);
			userOrderTo.setOrderItems(orderItemsTo);
			return userOrderTo;
		}catch(Exception e) {
			logger.error("Exception occured while adding new order {} ", e.getMessage());
			throw new OrderNotFoundException(e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see com.order.service.UserOrderService#get(java.lang.Long)
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public UserOrderTo get(Long orderId) throws OrderNotFoundException {
		logger.info("retrieving the userOrder by user order id {}", orderId);
		if(Objects.isNull(orderId)) {
			throw new OrderNotFoundException("orderId must not be null.");
		}
		UserOrderTo userOrderTo = null;
		try {
			 UserOrder userOrder = userOrderRepository.findById(orderId).orElse(null);
			 
			 if(Objects.isNull(userOrder)) {
				 return userOrderTo;
			 }
			
			 Object object = getAllItemsByOrderId(orderId);
			 if(object instanceof CustomeErrorResponse) {
				 throw new OrderNotFoundException("Exception occurred while fetching the order items");
			 } else {
				 List<UserOrderItemTo> orderItemTos = (List<UserOrderItemTo>) object;
				 userOrderTo = new UserOrderTo(userOrder, orderItemTos);
				 
			 }
			
			 return userOrderTo;
		}catch(Exception e) {
			logger.error("Exception occured while retriving the order {} ", e.getMessage());
			throw new OrderNotFoundException(e.getMessage());
		}
	}

	@Override
	public Object getAllItemsByOrderId(Long orderId) throws OrderNotFoundException {
		
		logger.info("retrieving the user's Order items by user order id {}", orderId);
		if(Objects.isNull(orderId)) {
			throw new OrderNotFoundException("order id must not be null.");
		}
		try {
			return orderItemServiceProxy.orderItemInfobyOrderId(orderId).getBody();
		}catch(Exception e) {
			logger.error("Exception occured while retriving the order {} ", e.getMessage());
			throw new OrderNotFoundException(e.getMessage());
		}
		
	}

}
