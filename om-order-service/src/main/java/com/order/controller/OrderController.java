package com.order.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.order.exception.OrderNotFoundException;
import com.order.service.UserOrderService;
import com.order.to.UserOrderTo;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/v1")
public class OrderController {

	private static final  Logger logger = LoggerFactory.getLogger(OrderController.class);

	@Autowired
	private UserOrderService userOrderService;
	
	@ApiOperation(value = "add new order info")
	@ApiResponses({ // swagger - describe return status code
		@ApiResponse(code = 200, message = "Success. Request completed."),
		@ApiResponse(code = 400, message = "BAD Request"),
		@ApiResponse(code = 404, message = "Not Found - resource doesn't exist."),
		@ApiResponse(code = 500, message = "Internal Server error.") })
	@PostMapping("/order")
	public ResponseEntity<UserOrderTo> saveNewOrder(@Valid @RequestBody UserOrderTo userOrderTo) throws OrderNotFoundException {
		try {
			return new ResponseEntity<>(userOrderService.save(userOrderTo), HttpStatus.CREATED);
		} catch (OrderNotFoundException e) {
			logger.error("Exception occured while adding new order : ", e);
			throw e;
		}
	}
	
	@ApiOperation(value = "get order info by id")
	@ApiResponses({ // swagger - describe return status code
		@ApiResponse(code = 200, message = "Success. Request completed."),
		@ApiResponse(code = 400, message = "BAD Request"),
		@ApiResponse(code = 404, message = "Not Found - resource doesn't exist."),
		@ApiResponse(code = 500, message = "Internal Server error.") })
	@GetMapping("/order/{orderid}")
	public ResponseEntity<UserOrderTo> orderInfobyId(@PathVariable("orderid") Long orderId) throws OrderNotFoundException {
		try {
			return new ResponseEntity<>(userOrderService.get(orderId),HttpStatus.OK);
		} catch (OrderNotFoundException onfe) {
			logger.error("Exception occured while retrieving order.{}", onfe);
			throw onfe;
		}
	}
	
	@ApiOperation(value = "get order items info by id")
	@ApiResponses({ // swagger - describe return status code
		@ApiResponse(code = 200, message = "Success. Request completed."),
		@ApiResponse(code = 400, message = "BAD Request"),
		@ApiResponse(code = 404, message = "Not Found - resource doesn't exist."),
		@ApiResponse(code = 500, message = "Internal Server error.") })
	@GetMapping("/order/items/{orderid}")
	public ResponseEntity<?> orderItemsInfobyId(@PathVariable("orderid") Long orderId) throws OrderNotFoundException {
		try {
			return new ResponseEntity<>(userOrderService.getAllItemsByOrderId(orderId),HttpStatus.OK);
		} catch (OrderNotFoundException onfe) {
			logger.error("Exception occured while retrieving order items by order id.{}", onfe);
			throw onfe;
		}
	}

}
