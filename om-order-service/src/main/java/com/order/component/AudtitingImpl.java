package com.order.component;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;

import org.springframework.stereotype.Component;

@Component
public class AudtitingImpl implements AuditorAware<String> {
	@Override
	public Optional<String> getCurrentAuditor() {

		return Optional.of("admin@order-management.com");

	}
}
