package com.order.config;


import java.time.Duration;
import java.time.Instant;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component("omaspect")
public class PerformanceAspect {
	
	private static final Logger logger = LoggerFactory.getLogger(PerformanceAspect.class);
	
	@Around("execution(* com.order.controller..*(..))")
	public Object logTime(ProceedingJoinPoint pjp) throws Throwable{
		
		Instant startTime = Instant.now();
		Object object = pjp.proceed();
		
		Instant endTime = Instant.now();
		Signature signature = pjp.getSignature();
		logger.info("Start Time of the method : " + signature + " is " + startTime);
		Duration duration = Duration.between(startTime, endTime);
		logger.info("End Time of the method : " + signature + " is " + endTime);
		logger.info("Time to run method : " + signature + " : millis " + duration.toString());
		
		if(duration.toMinutes() >= 2L) {
			logger.warn("The method : " + signature + "has been taken " + duration.toMinutes());
		}
		
		return object;
		
	}
	

}

