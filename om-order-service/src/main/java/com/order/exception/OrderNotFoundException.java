package com.order.exception;

public class OrderNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 859438602245039463L;

	public OrderNotFoundException(String message) {
		super(message);
	}

}
