package com.order.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.order.to.CustomeErrorResponse;

@RestControllerAdvice()
public class RestResponseEntityExceptionHandler 
  extends ResponseEntityExceptionHandler {
 
	 @ExceptionHandler(value = OrderNotFoundException.class)
	     public ResponseEntity<CustomeErrorResponse> handleGenericNotFoundException(OrderNotFoundException e) {
	         CustomeErrorResponse error = new CustomeErrorResponse("NOT_FOUND_ERROR", e.getMessage());
	         error.setTimestamp(new Date());
	         error.setStatus((HttpStatus.NOT_FOUND.value()));
	         return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	     } 
}
