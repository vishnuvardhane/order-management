/**
 * 
 */
package com.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.order.entity.UserOrder;

/**
 * @author lenovo
 *
 */
@Repository
public interface UserOrderRepository extends JpaRepository<UserOrder, Long> {

}
